from __future__ import division

from django.db import models
from django.utils.translation import ugettext_lazy as _

from shop.models.productmodel import Product


class PercentDiscountMixin(models.Model):
    """
    Apply ``amount`` percent discount to whole cart.
    """
    amount = models.DecimalField(_('Amount'), max_digits=5, decimal_places=2)

    def get_extra_cart_price_field(self, cart, request=None):
        amount = (self.amount/100) * cart.subtotal_price
        return (self.get_name(), amount,)

    class Meta:
        abstract = True


class RestrictedAbsoluteDiscountMixin(models.Model):
    """
    Apply ``amount`` discount to eligible_products in Cart.
    """
    amount = models.DecimalField(
        _('Amount'),
        max_digits=10,
        decimal_places=2,
    )
    restricted_to_products = models.ManyToManyField(
        Product,
        blank=True,
        help_text=_(
            'Which products should this discount apply to?  If any of this '
            'discount\'s eligible products appear in a customer\'s cart, it will '
            'be applied once to their subtotal.'
        ),
    )
    apply_to_all_products = models.BooleanField(
        default=False,
        help_text=_(
            'Should this discount apply to all products?  This would be '
            'equivalent to giving all customers a fixed discount for any '
            'purchase.'
        ),
    )

    def get_extra_cart_price_field(self, cart, request=None):
        if self.apply_to_all_products or self.has_eligible_product(cart, request):
            return (self.get_name(), self.amount,)

    def has_eligible_product(self, cart, request=None):
        eligible_products = set(self.restricted_to_products.values_list('id'))
        products = set(cart.items.values_list('product'))
        return bool(eligible_products & products)

    class Meta:
        abstract = True


class CartItemPercentDiscountMixin(models.Model):
    """
    Apply ``amount`` percent discount to eligible_products in Cart.
    """
    amount = models.DecimalField(_('Amount'), max_digits=5, decimal_places=2)

    def get_extra_cart_item_price_field(self, cart_item, request=None):
        if self.is_eligible_product(cart_item.product, cart_item.cart):
            return (self.get_name(),
                    self.calculate_discount(cart_item.line_subtotal))

    def calculate_discount(self, price):
        return (self.amount/100) * price

    class Meta:
        abstract = True


class CartItemAbsoluteDiscountMixin(models.Model):
    """
    Apply ``amount`` discount to eligible_products in Cart.
    """
    amount = models.DecimalField(_('Amount'), max_digits=5, decimal_places=2)

    def get_extra_cart_item_price_field(self, cart_item, request=None):
        if self.is_eligible_product(cart_item.product, cart_item.cart):
            return (self.get_name(),
                    self.calculate_discount(cart_item.line_subtotal))

    def calculate_discount(self, price):
        return self.amount

    class Meta:
        abstract = True
