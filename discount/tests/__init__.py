from discount.tests.base import (DiscountBaseTest, DiscountCartModifierTest,
        DiscountProductFiltersTest, CartItemPercentDiscountTest,
        CartItemAbsoluteDiscountTest, RestrictedAbsoluteDiscountTest)
from .view_tests import *
